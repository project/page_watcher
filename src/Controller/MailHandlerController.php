<?php

namespace Drupal\page_watcher\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\example\ExampleInterface;
use Drupal\page_watcher\Services\SubscriptionHandlerService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Page Watcher routes.
 */
class MailHandlerController extends ControllerBase {

  /**
   * The page_watcher.subscription_handler service.
   *
   * @var SubscriptionHandlerService
   */
  protected $pageWatcherSubscriptionHandler;

  /**
   * The controller constructor.
   *
   * @param \Drupal\page_watcher\Services\SubscriptionHandlerService $page_watcher_subscription_handler
   *   The page_watcher.subscription_handler service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(SubscriptionHandlerService $page_watcher_subscription_handler, EntityTypeManagerInterface $entityTypeManager) {
    $this->pageWatcherSubscriptionHandler = $page_watcher_subscription_handler;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('page_watcher.subscription_handler'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Confirm subscription
   */
  public function confirm($hash) {

    $storage = $this->entityTypeManager->getStorage('subscriber_entity');
    $subscriptions = $storage->loadByProperties([
      'token' => $hash,
      'status' => 0,
    ]);

    if (!$subscriptions) {
      $build['content'] = [
        '#type' => 'item',
        '#markup' => $this->t('This link has expired.'),
      ];
      return $build;
    }

    $subscription = reset($subscriptions);
    if ($subscription) {
      $subscription->status = 1;
      $subscription->hash = $this->pageWatcherSubscriptionHandler->generateHash();
      $subscription->save();
      $build['content'] = [
        '#type' => 'item',
        '#markup' => $this->t('You have confirmed your subscription. You can unsubscribe via clicking the \'unsubscribe\' link in your inbox.'),
      ];
    }

    return $build;
  }

  /**
   * Unsubscribe article
   */
  public function unsubscribe($hash) {
    $storage = $this->entityTypeManager->getStorage('subscriber_entity');
    $subscriptions = $storage->loadByProperties([
      'token' => $hash,
      'status' => 1,
    ]);

    if (!$subscriptions) {
      $build['content'] = [
        '#type' => 'item',
        '#markup' => $this->t('This link has expired.'),
      ];
      return $build;
    }

    $subscription = reset($subscriptions);
    $subscription->delete();
    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('You have been unsubscribed.'),
    ];

    return $build;
  }

}
