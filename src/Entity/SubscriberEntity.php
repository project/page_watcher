<?php

namespace Drupal\page_watcher\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\Entity\User;

/**
 * Defines the Subscriber entity entity.
 *
 * @ingroup page_watcher
 *
 * @ContentEntityType(
 *   id = "subscriber_entity",
 *   label = @Translation("Subscriber entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\page_watcher\SubscriberEntityListBuilder",
 *     "views_data" = "Drupal\page_watcher\Entity\SubscriberEntityViewsData",
 *     "translation" = "Drupal\page_watcher\SubscriberEntityTranslationHandler",
 *     "access" = "Drupal\page_watcher\SubscriberEntityAccessControlHandler",
 *   },
 *   base_table = "subscriber_entity",
 *   data_table = "subscriber_entity_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer subscriber entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 * )
 */
class SubscriberEntity extends ContentEntityBase implements SubscriberEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the user who has subscribed.'))
      ->setSettings(
        [
          'max_length' => 50,
          'text_processing' => 0,
        ]
      )
      ->setDefaultValue('')
      ->setDisplayOptions(
        'view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -4,
        ]
      )
      ->setDisplayOptions(
        'form', [
          'type' => 'string_textfield',
          'weight' => -4,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['token'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Token'))
      ->setDescription(t('Security token.'))
      ->setSettings(
        [
          'max_length' => 32,
          'text_processing' => 0,
        ]
      );

    $fields['e_mail_address'] = BaseFieldDefinition::create('email')
      ->setLabel(t('The email address'))
      ->setDescription(t('The e-mail address to send the notification from a subscription.'))
      ->setDefaultValue('')
      ->setDisplayOptions(
        'view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -4,
        ]
      )
      ->setDisplayOptions(
        'form', [
          'type' => 'string_textfield',
          'weight' => -4,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the subscriber entity is published.'))
      ->setDisplayOptions(
        'form', [
          'type' => 'boolean_checkbox',
          'weight' => -3,
        ]
      );

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['node_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Node id'))
      ->setDescription(t('The reference to the subscribed node.'))
      ->setSetting('target_type', 'node')
      ->setSetting('handler', 'default')
      ->setRevisionable(FALSE)
      ->setRequired(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User id'))
      ->setDescription(t('The reference to the user.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setRequired(TRUE)
      ->setRevisionable(FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    $nameValue = $this->get('name')->value;
    if ($nameValue) {
      return $nameValue;
    }
    $typedData = $this->get('user_id')->first();
    if (!$typedData) {
      return '';
    }
    $nameReference = $typedData->getValue()['target_id'];
    /**
     * @var $user User
     */
    $user = User::load($nameReference);
    return $user->getDisplayName();
  }

  public function getMail() {
    $mailValue = $this->get('e_mail_address')->value;
    if ($mailValue) {
      return $mailValue;
    }
    $mailReference = $this->get('user_id')->first()->getValue()['target_id'];
    /**
     * @var $user User
     */
    $user = User::load($mailReference);
    return $user->getEmail();
  }

  public function getHash() {
    return $this->get('token')->value;
  }

  public function getNodeId() {
    return $this->get('node_id') ? (int) $this->get('node_id')
      ->first()
      ->getValue()['target_id'] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

}
