<?php

namespace Drupal\page_watcher\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Subscriber entity entities.
 *
 * @ingroup page_watcher
 */
interface SubscriberEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Subscriber entity name.
   *
   * @return string
   *   Name of the Subscriber entity.
   */
  public function getName();

  /**
   * Sets the Subscriber entity name.
   *
   * @param string $name
   *   The Subscriber entity name.
   *
   * @return \Drupal\page_watcher\Entity\SubscriberEntityInterface
   *   The called Subscriber entity entity.
   */
  public function setName($name);

  /**
   * Gets the Subscriber entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Subscriber entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Subscriber entity creation timestamp.
   *
   * @param int $timestamp
   *   The Subscriber entity creation timestamp.
   *
   * @return \Drupal\page_watcher\Entity\SubscriberEntityInterface
   *   The called Subscriber entity entity.
   */
  public function setCreatedTime($timestamp);

}
