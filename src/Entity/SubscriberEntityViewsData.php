<?php

namespace Drupal\page_watcher\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Subscriber entity entities.
 */
class SubscriberEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
