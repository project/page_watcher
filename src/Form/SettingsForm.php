<?php

namespace Drupal\page_watcher\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\NodeType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\page_watcher\Form
 */
class SettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }


  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Define the page watcher settings file name.
   */
  protected const SETTINGS_FILENAME = 'page_watcher.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'page_watcher_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::SETTINGS_FILENAME);

    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    // If you need to display them in a drop down:
    $options = [];
    foreach ($node_types as $node_type) {
      $options[$node_type->id()] = $node_type->label();
    }

    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content types'),
      '#description' => $this->t('Select content type'),
      '#options' => $options,
      '#default_value' => $config->get('content_types'),
      '#weight' => '0',
    ];

    $form['page_watcher_mail_section'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Page watcher default mail text'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      'token_tree' => [
        '#theme' => 'token_tree_link',
        '#token_types' => ['page_watcher_subscriber', 'node'],
        '#show_restricted' => TRUE,
      ],
    ];

    $form['page_watcher_mail_section']['default_mail_subject_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default mail subject'),
      '#description' => $this->t('The default mail subject if none specific subject is defined.'),
      '#default_value' => $config->get('default_mail_subject_text'),
      '#attributes' => [
        'placeholder' => $this->t('Default mail subject'),
      ],
    ];

    $form['page_watcher_mail_section']['default_mail_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Default mail text'),
      '#description' => $this->t($this->getDescriptionText()),
      '#default_value' => $config->get('default_mail_text'),
      '#attributes' => [
        'placeholder' => $this->t('Default mail text'),
      ],
    ];

    $form['page_watcher_mail_section']['notice_mail'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Default notice mail text'),
      '#description' => $this->t($this->getDescriptionText()),
      '#default_value' => $config->get('notice_mail'),
      '#attributes' => [
        'placeholder' => $this->t('Default notice mail text'),
      ],
    ];

    $form['page_watcher_mail_section']['default_e_mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Default e-mail address'),
      '#description' => $this->t('The default consignor e-mail address.'),
      '#default_value' => $config->get('default_e_mail'),
      '#attributes' => [
        'placeholder' => $this->t('Default e-mail address'),
      ],
    ];

    $form['page_watcher_modal_settings_section'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Page watcher modal settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      'token_tree' => [
        '#theme' => 'token_tree_link',
        '#token_types' => ['page_watcher_subscriber', 'node'],
        '#show_restricted' => TRUE,
      ],
    ];

    $form['page_watcher_modal_settings_section']['default_modal_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Modal help text'),
      '#description' => $this->t('A help text for the user subscribe modal.'),
      '#
      ' => $config->get('default_modal_text'),
      '#attributes' => [
        'placeholder' => $this->t('Modal help text'),
      ],
    ];

    $form['save_config_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#attributes' => [
        'class' => ['btn', 'btn-primary'],
      ],
    ];

    return $form;
  }

  /**
   * Gets the description text for the frontend.
   *
   * @return string
   */
  private function getDescriptionText(): string {
    return $this->t('The default mail text if none specific text is defined.
      If you want to set automatic replacements, then use some of the available tokens.');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::SETTINGS_FILENAME);

    $allowedConfigKeys = [
      'default_e_mail',
      'default_mail_subject_text',
      'default_mail_text',
      'default_modal_text',
      'notice_mail',
      'content_types',
    ];
    foreach ($form_state->getValues() as $key => $value) {
      if (!in_array($key, $allowedConfigKeys, TRUE)) {
        continue;
      }
      $config->set($key, $value);
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * @inheritDoc
   */
  protected function getEditableConfigNames() {
    return [self::SETTINGS_FILENAME];
  }

}
