<?php

namespace Drupal\page_watcher\Form;

use Drupal;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\page_watcher\Services\SubscriptionHandlerService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * SendToDestinationsForm class.
 */
class SubscriptionModalDialogForm extends FormBase {

  /**
   * @var SubscriptionHandlerService
   */
  private $subscriptionHandler;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $account;

  /**
   * SubscriptionModalDialogForm constructor.
   *
   * @param \Drupal\page_watcher\Services\SubscriptionHandlerService $subscriptionHandler
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function __construct(SubscriptionHandlerService $subscriptionHandler, RouteMatchInterface $routeMatch, AccountInterface $account) {
    $this->subscriptionHandler = $subscriptionHandler;
    $this->routeMatch = $routeMatch;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('page_watcher.subscription_handler'),
      $container->get('current_route_match'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'page_watcher_subscribe_modal_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $options = NULL) {
    $form['#prefix'] = '<div id="modal_example_form">';
    $form['#suffix'] = '</div>';
    // The status messages that will contain any form errors.
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];
    if (!$this->account->id()) {
      $form['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
      ];

      $form['email'] = [
        '#type' => 'email',
        '#title' => $this->t('E-mail'),
        '#required' => TRUE,
      ];
    }
    $form['text'] = [
      '#markup' => $this->t('Do you want to be noticed if this article will be updated?'),
    ];
    $form['accept'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Accept'),
      '#description' => $this->t('Do you want to be noticed if this article will be updated?'),
      '#required' => TRUE,
    ];
    $form['node_id'] = [
      '#type' => 'hidden',
    ];
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];
    $form['mandatory'] = [
      '#markup' => $this->t('Fields marked with a \'*\' are mandatory'),
    ];
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();
    // If there are any form errors, AJAX replace the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#modal_example_form', $form));
    }
    else {
      $response->addCommand(new OpenModalDialogCommand($this->t('Success!'), $this->t('To subscribe to the newsletter, please confirm the email we sent. You can cancel the newsletter at any time using the "unsubscribe" link.'), ['width' => 700]));
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->account->id()) {
      $form_state->setValue('user_id', $this->account->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = [
      'name' => $form_state->getValue('name'),
      'e_mail_address' => $form_state->getValue('email'),
      'status' => 0,
      'node_id' => $form_state->getValue('node_id'),
      'user_id' => $form_state->getValue('user_id'),
    ];

    /** @var $subscriber \Drupal\page_watcher\Entity\SubscriberEntity */
    if (($subscriber = $this->subscriptionHandler->addSubscriber($values))) {

      /** @var $mailBuilder \Drupal\page_watcher\Services\MailBuilder */
      $mailBuilder = Drupal::service('page_watcher.services.mail_builder');
      $data['message'] = [];
      $data['key'] = 'page_update';
      $mailBuilder->preprocessMailText($data['message'], 'default_mail_text', $subscriber);

      $queue = Drupal::service('queue')->get('page_watcher_mailqueue');
      $queue->createQueue();
      /**
       * @var $subscriber \Drupal\page_watcher\Entity\SubscriberEntity
       */
      $data['email'] = $subscriber->getMail();
      $data['subscriber_entity'] = $subscriber;
      $data['key'] = 'subscribe';
      $queue->createItem($data);
      return;
    }
  }

}
