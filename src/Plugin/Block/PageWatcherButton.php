<?php

namespace Drupal\page_watcher\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'PageWatcherButton' block.
 *
 * @Block(
 *  id = "page_watcher_button",
 *  admin_label = @Translation("Page watcher button"),
 * )
 */
class PageWatcherButton extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  private $routeMatch;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, CurrentRouteMatch $routeMatch) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
    $this->routeMatch = $routeMatch;
  }

  /**
   * Instantiates a new instance of this class.
   *
   * This is a factory method that returns a new instance of this class. The
   * factory should pass any needed dependencies into the constructor of this
   * class, but not the container itself. Every call to this method must return
   * a new instance of this class; that is, it may not implement a singleton.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this instance should use.
   *
   * @return \Drupal\page_watcher\Plugin\Block\PageWatcherButton
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = $this->routeMatch->getParameter('node');
    if (!$node) {
      return NULL;
    }
    $link_url = Url::fromRoute('page_watcher.modal', ['node_id' => $node->id()]);
    $link_url->setOptions([
      'attributes' => [
        'class' => [
          'use-ajax',
          'button',
          'button--small',
          'page-watcher-button',
        ],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 400]),
      ],
    ]);

    return [
      '#type' => 'markup',
      '#markup' => Link::fromTextAndUrl($this->t('Subscribe page'), $link_url)
        ->toString(),
      '#attached' => ['library' => ['core/drupal.dialog.ajax']],
    ];
  }

  public function access(AccountInterface $account, $return_as_object = FALSE) {
    /**
     * @var NodeInterface $node
     */
    $node = $this->routeMatch->getParameter('node');

    $contentTypes = $this->configFactory
      ->get('page_watcher.settings')
      ->get('content_types');
    if ($node && $account->hasPermission('can use page watcher') && $contentTypes[$node->bundle()]) {
      return parent::access($account, $return_as_object);
    }
    return AccessResult::forbidden('Shouldn\'t be accessible on current page');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
