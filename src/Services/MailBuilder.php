<?php


namespace Drupal\page_watcher\Services;


use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\node\NodeInterface;
use Drupal\page_watcher\Entity\SubscriberEntity;
use Drupal\token\Token;

class MailBuilder implements MailBuilderInterface {


  /**
   * @var \Drupal\token\Token
   */
  private $token;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @inheritDoc
   */
  public function __construct(Token $token, ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager) {
    $this->token = $token;
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * @inheritDoc
   */
  public function getMailAddress(SubscriberEntity $subscriberEntity): string {
    return $subscriberEntity->getMail();
  }

  /**
   * @inheritDoc
   */
  public function getUsername(SubscriberEntity $subscriberEntity): string {
    return $subscriberEntity->getName();
  }

  public function preprocessMailText(array &$message, string $textId, SubscriberEntity $subscriberEntity): array {
    /** @var $mailBuilder \Drupal\page_watcher\Services\MailBuilderInterface */
    $settings = $this->configFactory->get('page_watcher.settings');
    $settingsRaw = $settings->getRawData();
    $message['format'] = 'text/html';
    $message['headers']['Content-Type'] = SWIFTMAILER_FORMAT_HTML;
    $message['subject'] = $this->preprocess($subscriberEntity, $settingsRaw['default_mail_subject_text']);
    $body = $this->preprocess($subscriberEntity, $settingsRaw[$textId]);
    $message['body'][] = Markup::create($body);
    $message['from'] = $settingsRaw['default_e_mail'];
    return $message;
  }

  /**
   * @inheritDoc
   */
  public function preprocess(SubscriberEntity $subscriberEntity, string $text): string {
    $node = $this->getNode($subscriberEntity->getNodeId());
    if ($node) {
      return $this->token->replace($text, [
        'subscriber_entity' => $subscriberEntity,
        'node' => $node,
      ]);
    }
    return $this->token->replace($text, [
      'subscriber_entity' => $subscriberEntity,
    ]);
  }

  private function getNode($nodeId): ?NodeInterface {
    if (!$nodeId) {
      NULL;
    }

    /**
     * @var $node NodeInterface|null
     */
    $node = $this->entityTypeManager->getStorage('node')->load($nodeId);
    if ($node) {
      return $node;
    }
    return NULL;
  }

}
