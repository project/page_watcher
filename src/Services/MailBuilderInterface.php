<?php

namespace Drupal\page_watcher\Services;

use Drupal\page_watcher\Entity\SubscriberEntity;

interface MailBuilderInterface {

  /**
   * Returns users mail address.
   *
   * @param \Drupal\page_watcher\Entity\SubscriberEntity $subscriberEntity
   *
   * @return string
   */
  public function getMailAddress(SubscriberEntity $subscriberEntity): string;

  /**
   * Returns username.
   *
   * @param \Drupal\page_watcher\Entity\SubscriberEntity $subscriberEntity
   *
   * @return string
   */
  public function getUsername(SubscriberEntity $subscriberEntity): string;

  /**
   * Returns mail subject.
   *
   * @param \Drupal\page_watcher\Entity\SubscriberEntity $subscriberEntity
   * @param string $text
   *
   * @return string
   */
  public function preprocess(SubscriberEntity $subscriberEntity, string $text): string;

}
