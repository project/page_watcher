<?php

namespace Drupal\page_watcher\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;

class SubscriptionHandlerService {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  private $mailManager;

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;


  /**
   * SubscribtionHandlerService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, MailManagerInterface $mailManager, LanguageManagerInterface $languageManager) {

    $this->entityTypeManager = $entityTypeManager;
    $this->mailManager = $mailManager;
    $this->languageManager = $languageManager;
  }

  /**
   * Adds a subscriber
   *
   * @param array $values
   *
   * @return bool|\Drupal\Core\Entity\EntityInterface|bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addSubscriber(array $values) {
    if ($this->checkUnique($values['email'] ?? NULL, $values['user_id'] ?? NULL, $values['node_id'])) {
      $entity = $this->entityTypeManager->getStorage('subscriber_entity')
        ->create($values + ['token' => $this->generateHash()]);
      $entity->save();
      return $entity;
    }
    return FALSE;
  }

  /**
   * checks for unique mail address and user
   *
   * @param string|null $email
   * @param string|null $userId
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function checkUnique(?string $email, ?string $userId, $nodeId): bool {
    return ($this->checkUniqueDisabled('e_mail_address', $email, $nodeId) && $this->checkUniqueDisabled('user_id', $userId, $nodeId));
  }

  /**
   * @param string $fieldName
   * @param $fieldValue
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function checkUniqueDisabled(string $fieldName, $fieldValue, $nodeId): bool {
    $storage = $this->entityTypeManager->getStorage('subscriber_entity');
    /** @var \Drupal\page_watcher\Entity\SubscriberEntity[]|null $subscription */
    $subscriptions = $storage->loadByProperties([
      $fieldName => (string) $fieldValue,
      'node_id' => $nodeId,
    ]);

    if ($subscriptions) {
      $subscription = reset($subscriptions);
    }
    else {
      return TRUE;
    }
    if ($subscription->isPublished()) {
      return FALSE;
    }
    $subscription->delete();
    return TRUE;
  }

  /**
   * @return string
   */
  public function generateHash(): string {
    return md5(uniqid(mt_rand(), TRUE));
  }

  public function getSubscribers(int $nodeId): array {
    $storage = $this->entityTypeManager->getStorage('subscriber_entity');
    return $storage->loadByProperties(['status' => 1, 'node_id' => $nodeId]);
  }

}
