<?php

namespace Drupal\page_watcher;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Subscriber entity entity.
 *
 * @see \Drupal\page_watcher\Entity\SubscriberEntity.
 */
class SubscriberEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\page_watcher\Entity\SubscriberEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished subscriber entity entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published subscriber entity entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit subscriber entity entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete subscriber entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add subscriber entity entities');
  }

}
