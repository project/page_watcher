<?php

namespace Drupal\page_watcher;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for subscriber_entity.
 */
class SubscriberEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
