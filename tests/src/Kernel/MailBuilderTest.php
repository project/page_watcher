<?php

namespace Drupal\Tests\page_watcher\Kernel;

use Drupal;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\page_watcher\Entity\SubscriberEntity;
use Drupal\page_watcher\Services\MailBuilderInterface;
use Drupal\user\Entity\User;
use Drupal\vergabe_newsletter_asset\Services\TokenReplacer;

/**
 * Class TokenReplacerTest.
 *
 * @covers \Drupal\page_watcher\Services\TokenReplacer
 * @group  page_watcher
 *
 * @package Drupal\Tests\vergabe_newsletter_asset\phpunit\Kernel
 */
class MailBuilderTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'page_watcher',
    'token',
    'node',
    'user',
    'system',
  ];

  /**
   * @var MailBuilderInterface
   */
  private $mailBuilder;

  /**
   * @var SubscriberEntity
   */
  private $guestSubscriber;

  /**
   * @var SubscriberEntity
   */
  private $userSubscriber;

  /**
   * @var User
   */
  private $testUser;

  /**
   * @var \Drupal\page_watcher\Services\SubscriptionHandlerService
   */
  private $subscriptionHandler;

  /**
   * @var \Drupal\node\NodeInterface
   */
  private $node;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->mailBuilder = Drupal::service('page_watcher.services.mail_builder');
    $this->subscriptionHandler = Drupal::service('page_watcher.subscription_handler');
    $this->installEntitySchema('subscriber_entity');
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installSchema('system', ['sequences']);
    $this->generateNode();

    $guestSubscriberData = [
      'name' => 'Test1',
      'e_mail_address' => 'test@example.com',
      'status' => 0,
      'node_id' => $this->node->id(),
    ];
    $this->guestSubscriber = SubscriberEntity::create($guestSubscriberData);
    $this->guestSubscriber->save();

    $testUserData['name'] = 'KanielDanzen';
    $testUserData['mail'] = $testUserData['name'] . '@example.com';
    $testUserData['status'] = 1;

    $this->testUser = User::create($testUserData);
    $this->testUser->save();

    $userSubscriberData = [
      'user_id' => $this->testUser->id(),
      'status' => 0,
      'node_id' => $this->node->id(),
      'token' => $this->subscriptionHandler->generateHash(),
    ];
    $this->userSubscriber = SubscriberEntity::create($userSubscriberData);
    $this->userSubscriber->save();
  }

  private function generateNode() {
    $node_type = NodeType::create(
      [
        'type' => 'article',
        'label' => 'Article',
      ]
    );
    $node_type->save();
    $node = Node::create(
      [
        'type' => 'article',
        'title' => 'Test',
      ]
    );
    $node->save();
    $this->node = $node;
  }

  /**
   * @testdox Ensure the service could be injected.
   */
  public function testCanInjectTheService() {
    $this->assertInstanceOf(MailBuilderInterface::class, $this->mailBuilder, 'Service could not be injected.');
  }

  /**
   * @testdox Ensure that mail text can be replaced
   */
  public function testMailTextCanBeReplaced() {
    $text = $this->mailBuilder->preprocess($this->guestSubscriber, 'Hello [subscriber_entity:name], further text...');
    self::assertEquals('Hello Test1, further text...', $text);
  }

  /**
   * @testdox Ensure that username can be replaced if user reference is used
   */
  public function testMailTextCanBeReplacedWithUserReference() {
    $text = $this->mailBuilder->preprocess($this->userSubscriber, 'Hello [subscriber_entity:name], further text...');
    self::assertEquals('Hello KanielDanzen, further text...', $text);
  }

  /**
   * @testdox Ensure that mail text can be extracted
   */
  public function testMailCanBeExtraced() {
    $mailAddress = $this->mailBuilder->getMailAddress($this->guestSubscriber);
    self::assertEquals('test@example.com', $mailAddress);
  }

  /**
   * @testdox Ensure that mail can be extracted by user reference.
   */
  public function testMailCanBeExtracedWithUserReference() {
    $mailAddress = $this->mailBuilder->getMailAddress($this->userSubscriber);
    self::assertEquals($mailAddress, 'KanielDanzen@example.com');
  }

  /**
   * @testdox Ensure that correct link will be generated
   */
  public function testUnsubscribeLinkGenerator() {
    $link = $this->mailBuilder->preprocess($this->userSubscriber, '[subscriber_entity:unsubscribe_link]');
    self::assertEquals($link, 'http://localhost/page-watcher/unsubscribe/' . $this->userSubscriber->getHash());
  }

  /**
   * @testdox Ensure that correct link will be generated
   */
  public function testSubscribeLinkGenerator() {
    $link = $this->mailBuilder->preprocess($this->userSubscriber, '[subscriber_entity:subscribe_link]');
    self::assertEquals($link, 'http://localhost/page-watcher/confirm/' . $this->userSubscriber->getHash());
  }

  /**
   * @testdox Ensure that node title can be replaced
   */
  public function testNodeTokens() {
    $link = $this->mailBuilder->preprocess($this->userSubscriber, '[subscriber_entity:name][node:title]');
    self::assertEquals($link, 'KanielDanzenTest');
  }


}
